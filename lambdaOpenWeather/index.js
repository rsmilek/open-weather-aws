const https = require("https");
const util = require("util");

const openWeatherUrlFmt = "https://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appid=ca0da24355ce3bd31d840faba9d4c40e";

// Gets JSON string data from Open Weather API for given custom URL
const getOpenWeatherData = (url) => {
    return new Promise((resolve, reject) => {
        https.get(url, (res) => {
            res.setEncoding('utf8');
            let body = "";
            res.on('data', (data) => body += data);
            res.on("end", () => resolve(body));
        }).on('error', (error) => reject(error));
    });
};

// AWS Lamda function handler to return temperature value for given 'event.location'
exports.handler = async (event) => {
    const { location } = event;
    if (!location) location = "Prague";
    let openWeatherUrl = util.format(openWeatherUrlFmt, location);

    return await getOpenWeatherData(openWeatherUrl)
        .then((result) => {
            const response = {
                statusCode: 200,
                body: {
                    "datetime": Date.now(),
                    "temperature": JSON.parse(result).main.temp
                }
            };
            return response;
        }).catch((error) => {
            const response = {
                statusCode: 403,
                body: error
            };
            return response;
        });
};
