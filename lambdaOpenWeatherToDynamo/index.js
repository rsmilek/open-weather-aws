const AWS = require('aws-sdk');
const lambda = new AWS.Lambda;

// Invoke's given Lambda function 'lambdaFunctionName' with paramters 'payload'
const invokeLambda = async (lambdaFunctionName, payload) => {
   // Covert 'payload' to JSON string if it isn't
   let payloadStr;
   if (typeof payload === 'string') 
       payloadStr = payload;
   else
       payloadStr = JSON.stringify(payload, null, 2);
   // Build Lambda function invocation parameters
   let params = {
       FunctionName   : lambdaFunctionName,               /* string type, required */
       // ClientContext  : '',                               /* 'STRING_VALUE' */
       InvocationType : 'RequestResponse',                /* string type: 'Event' (async)| 'RequestResponse' (sync) | 'DryRun' (validate parameters y permissions) */
       // InvocationType : 'Event',
       LogType        : 'None',                           /* string type: 'None' | 'Tail' */
       // LogType        : 'Tail',
       Payload        : payloadStr,                       /* Buffer.from('...') || 'JSON_STRING' */ /* Strings will be Base-64 encoded on your behalf */
       //  Qualifier      : '',                             /* STRING_VALUE' */
   };
   const lambdaResult = await lambda.invoke(params).promise();
   console.log('invokeLambdaSync::lambdaResult: ', lambdaResult);
   // If you use LogType = 'Tail', you'll obtain the logs in lambdaResult.LogResult.
   // If you use 'None', there will not exist that field in the response.
   if (lambdaResult.LogResult)
       console.log('Logs of lambda execution: ',  Buffer.from(lambdaResult.LogResult, 'base64').toString());
   return lambdaResult; // The actual value returned by the lambda it is lambdaResult.Payload, there are other fields (some of them are optional)
};


// Store's given 'data' into DynamoDb table 'TabOpenWeather'
const putDataToDynamoDb = async (data) => {
   const documentClient = new AWS.DynamoDB.DocumentClient();
   // Prepare parameters
   const {datetime, temperature} = data.body;
   const params = {
      TableName: "TabOpenWeather",
      Item: {
         locationId_observable: "Prague_temperature",
         datetime: datetime,
         observable: "temperature",
         value: temperature  
      }
   };
   // Write to DB's table
   try {
      await documentClient.put(params).promise();
      return  params;
   } catch (err) {
      return err;
   }
};


// AWS Lamda function handler
exports.handler = async (event) => {
   const res = invokeLambda('lambdaOpenWeather', { "location": "Prague" })
   .then((res) => { 
      return putDataToDynamoDb(JSON.parse(res.Payload));
   })
   .then ((res) => {
      return {
         statusCode: 201,
         body: res
      };
   })
   .catch((error) => { 
      return { 
         statusCode: 403, 
         body: error
      };
   });
   return res;
};
